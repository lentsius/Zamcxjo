extends Node

var nunakanto = null

func _ready():
	nunakanto = $m2

func ludu_hazardan():
	randomize()
	var kantoj = get_children()
	var hazard = randi() % kantoj.size()
	
	kantoj[hazard].play()
	nunakanto = kantoj[hazard]

func _on_m1_finished():
	ludu_hazardan()


func _on_m2_finished():
	ludu_hazardan()


func _on_m3_finished():
	ludu_hazardan()
