tool
extends StaticBody

var tipo = 1
var teksto = ""

var l1_klakis = false
var l2_klakis = false

var rapideco = 10

var mis_poentoj

func _ready():
	$vidpunkto/kontrolo/teksto.text = teksto
	get_parent().vortoj_en_la_ludo += 1

func _process(delta):
	var movi = Vector3(0, -rapideco * delta * globala.rapideco, 0)
	
	translate(movi)

func vortigi(tio):
	teksto = tio
	$vidpunkto/kontrolo/teksto.text = tio
	
func modifi_rapidecon(kiom):
	rapideco = kiom
	
func frue(ludanto, elektita_tipo, poentoj):
	if elektita_tipo == tipo:
		if ludanto == 1:
			print("L1 DIVENITE!")
			l1_klakis = true
			globala.l1_poentoj += poentoj
			globala.l1_partikloj(2, "frue")
			globala.l1_frue()
		else:
			print("L2 DIVENITE!")
			l2_klakis = true
			globala.l2_poentoj += poentoj
			globala.l2_partikloj(2, "frue")
			globala.l2_frue()
	else:
		if ludanto == 1:
			print("L1 MALBONE!")
			l1_klakis = true
			globala.l1_mistrafis()

		else:
			print("L2 MALBONE!")
			l2_klakis = true
			globala.l2_mistrafis()
	
func bone(ludanto, elektita_tipo, poentoj):
	if elektita_tipo == tipo:
		if ludanto == 1:
			print("L1 DIVENITE!")
			l1_klakis = true
			globala.l1_poentoj += poentoj
			globala.l1_partikloj(10, "bone")
			globala.l1_bone()
		else:
			print("L2 DIVENITE!")
			l2_klakis = true
			globala.l2_poentoj += poentoj
			globala.l2_partikloj(10, "bone")
			globala.l2_bone()
	else:
		if ludanto == 1:
			print("L1 MALBONE!")
			l1_klakis = true
			globala.l1_mistrafis()

		else:
			print("L2 MALBONE!")
			l2_klakis = true
			globala.l2_mistrafis()

	
func mojose(ludanto, elektita_tipo, poentoj):
	if elektita_tipo == tipo:
		if ludanto == 1:
			print("L1 DIVENITE!")
			l1_klakis = true
			globala.l1_poentoj += poentoj
			globala.l1_partikloj(60, "mojose")
			globala.l1_mojose()
		else:
			print("L2 DIVENITE!")
			l2_klakis = true
			globala.l2_poentoj += poentoj
			globala.l2_partikloj(60, "mojose")
			globala.l2_mojose()
	else:
		if ludanto == 1:
			print("L1 MALBONE!")
			l1_klakis = true
			globala.l1_mistrafis()

		else:
			print("L2 MALBONE!")
			l2_klakis = true
			globala.l2_mistrafis()

	
func maltrafi():
	get_parent().vortoj_en_la_ludo -= 1
	get_parent().kontroli_vortojn()
	if !l1_klakis:
		pass
	if !l2_klakis:
		pass
	self.queue_free()		
	globala.nuligi_vorton()
	