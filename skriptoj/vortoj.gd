extends Spatial

var mia_fono

var gxustaj_vortoj = []
var malgxustaj_vortoj = []
var volapukajxoj = []

var uzitaj_gxustaj_vortoj = []
var uzitaj_malgxustaj_vortoj = []
var uzitaj_volapukajxoj = []

var novvorta_rapideco = 10

var finlisto = []

var nuna_vorto = 0

var nombro_da_vortoj = 0

var vorteblas = true

var vortoj_en_la_ludo = 0

var tween_agordoj = [Color(1,1,1,0), Color(1,1,1,1)]
var jam_ludis_tween = false

func _ready():
	randomize()
	gxustaj_vortoj = preni_gxustajn_vortojn()
	limigi_gxustajn_vortojn(20)
	
	malgxustaj_vortoj = preni_malgxustajn_vortojn()
	limigi_malgxustajn_vortojn(20)
	
	volapukajxoj = preni_volapukajxojn()
	limigi_volapukajxojn(20)
		
	#ni kalkulu la sumon por scii kiom granda la fina listo estos
	nombro_da_vortoj = volapukajxoj.size() + gxustaj_vortoj.size() + malgxustaj_vortoj.size()

	while not finlisto.size() == nombro_da_vortoj * 2:
		randomize()
		var hazard_elekto = randi() % 3
		if hazard_elekto == 0:
			if gxustaj_vortoj.size() == 0:
				break
			finlisto.append(gxustaj_vortoj[0])
			uzitaj_gxustaj_vortoj.append(gxustaj_vortoj[0])			
			gxustaj_vortoj.remove(0)
			finlisto.append(1)
		elif hazard_elekto == 1:
			if malgxustaj_vortoj.size() == 0:
				break
			finlisto.append(malgxustaj_vortoj[0])
			uzitaj_malgxustaj_vortoj.append(malgxustaj_vortoj[0])
			malgxustaj_vortoj.remove(0)
			finlisto.append(2)
		elif hazard_elekto == 2:
			if volapukajxoj.size() == 0:
				break
			finlisto.append(volapukajxoj[0])
			uzitaj_volapukajxoj.append(volapukajxoj[0])
			volapukajxoj.remove(0)
			finlisto.append(3)

#	print(finlisto)

func _process(delta):
	pass
	
		
func _on_ofteco_timeout():
	if nuna_vorto < nombro_da_vortoj * 2 and vorteblas:
		nova_vorto()

func _on_komencekrano_start():
	nova_vorto()
	$ofteco.start()
	$tempo.start()
	$mezo.start()
	$komenco.start()
	$fino.start()

func nova_vorto():
	var vortsceno = load('res://assets/vorto/vorto.tscn')
	var nova_vorto = vortsceno.instance()
	nova_vorto.teksto = finlisto[nuna_vorto]
	nova_vorto.tipo = finlisto[nuna_vorto + 1]
	nova_vorto.rapideco = novvorta_rapideco
	add_child(nova_vorto)
#	call_deferred("add_child", nova_vorto)
	nuna_vorto += 2	
	
func _on_tempo_timeout():
	vorteblas = false
	
func kontroli_vortojn():
	if vortoj_en_la_ludo < 1:
		$'../ludo'.fino()

#agordoj sxangxigxas dum la ludo

func _on_komenco_timeout():
	$ofteco.wait_time = 2
	novvorta_rapideco = 10

func _on_mezo_timeout():
	$ofteco.wait_time = 1.5
	novvorta_rapideco = 10

func _on_fino_timeout():
	$Tween.interpolate_property($avertkoloro, "modulate", tween_agordoj[0], tween_agordoj[1], 1,Tween.TRANS_CUBIC,Tween.EASE_IN_OUT, 0)
	$Tween.start()
	$avertkoloro.visible = true
	$alarmo.play()
	
### VORT PRENADO
func preni_gxustajn_vortojn():
	var dosiero = File.new()
	dosiero.open("res://vortoj/gxustaj.txt", File.READ)
	var prenvortoj = []
	while !dosiero.eof_reached():
		prenvortoj.append(dosiero.get_line())
	dosiero.close()
	return prenvortoj
	
func preni_malgxustajn_vortojn():
	var dosiero = File.new()
	dosiero.open("res://vortoj/malgxustaj.txt", File.READ)
	var prenvortoj = []
	while !dosiero.eof_reached():
		prenvortoj.append(dosiero.get_line())
	dosiero.close()
	return prenvortoj
	
func preni_volapukajxojn():
	var dosiero = File.new()
	dosiero.open("res://vortoj/volapukajxoj.txt", File.READ)
	var prenvortoj = []
	while !dosiero.eof_reached():
		prenvortoj.append(dosiero.get_line())
	dosiero.close()
	return prenvortoj
	
func limigi_gxustajn_vortojn(numero):
	while gxustaj_vortoj.size() > numero:
		randomize()
		var hazardelekto = randi() % gxustaj_vortoj.size()
		gxustaj_vortoj.remove(hazardelekto)
	
func limigi_malgxustajn_vortojn(numero):
	while malgxustaj_vortoj.size() > numero:
		randomize()
		var hazardelekto = randi() % malgxustaj_vortoj.size()
		malgxustaj_vortoj.remove(hazardelekto)
	
func limigi_volapukajxojn(numero):
	while volapukajxoj.size() > numero:
		randomize()
		var hazardelekto = randi() % volapukajxoj.size()
		volapukajxoj.remove(hazardelekto)





func _on_Tween_tween_completed(object, key):
	if jam_ludis_tween:
		$avertkoloro.visible = false
		$ofteco.wait_time = 1
		novvorta_rapideco = 10
	else:
		tween_agordoj.invert()
		$Tween.interpolate_property($avertkoloro, "modulate", tween_agordoj[0], tween_agordoj[1], 1,Tween.TRANS_CUBIC,Tween.EASE_IN_OUT, 0)
		$Tween.start()
		jam_ludis_tween = true
