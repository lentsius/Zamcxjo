extends Control

var l1_pretas = false
var l2_pretas = false

func _ready():
	self.visible = true
	if globala.jam_ludis:
		komencu()

signal start

func _process(delta):
	#sistemaj butonoj, helpaj precipe por menuoj
	if Input.is_action_just_pressed("start") or Input.is_action_just_pressed("start_l2") or Input.is_action_just_pressed("ui_up"):
		ludu_sonon("klako")
		if l1_pretas and l2_pretas:
			komencu()

	if Input.is_action_just_pressed("select"):
		ludu_sonon("klako")		
		$sube_maldekstre/CenterContainer/TextureButton.pressed = !$sube_maldekstre/CenterContainer/TextureButton.pressed
		l1_pretas = !l1_pretas
		
	if Input.is_action_just_pressed("select_l2"):
		ludu_sonon("klako")
		$sube_maldekstre2/CenterContainer/TextureButton.pressed = !$sube_maldekstre2/CenterContainer/TextureButton.pressed
		l2_pretas = !l2_pretas
		
	if Input.is_action_just_pressed("cheat"):
		$sube_maldekstre2/CenterContainer/TextureButton.pressed = !$sube_maldekstre2/CenterContainer/TextureButton.pressed
		l2_pretas = !l2_pretas
		$sube_maldekstre/CenterContainer/TextureButton.pressed = !$sube_maldekstre/CenterContainer/TextureButton.pressed
		l1_pretas = !l1_pretas
		
	if l1_pretas and l2_pretas:
		$komencu.visible = true
	else:
		$komencu.visible = false
		
	if Input.is_action_just_pressed("right_down") or Input.is_action_just_pressed("right_down_l2"):
		$poenttabulo.visible = !$poenttabulo.visible
		
func ludu_sonon(sono):
	if sono == "klako":
		$'../sonoj/klako'.play()
		
func komencu():
	emit_signal("start")
	globala.ludo.pauzeblas = true
	$'../sonoj/ek'.play()
	queue_free()
	
func _input(event):
	if event is InputEventJoypadButton:
		print(event.button_index, event.as_text())