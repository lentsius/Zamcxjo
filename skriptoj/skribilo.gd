extends Node

var alfabeto = []
var kursoro = 0
var teksto = "VIA NOMO" # komenca teksto

func _init():
	var alfastring = "ABCĈDEFGĜHĤIJĴKLMNOPQRSŜTUŬVWXYZ "
	for c in alfastring:
		self.alfabeto.append(c)
		
func supresube(supre):
	# sxangxas la leteron je la kursoro kun la venonta (se supre = 1)
	# aux kun la antauxa (se supre = -1) laux la alfabeto
	var letero = self.teksto.substr(self.kursoro, 1)
	var nova_letero_index = (self.alfabeto.find(letero) + supre) % len(self.alfabeto)
	var nova_letero = self.alfabeto[nova_letero_index]
	
	self.teksto = self.teksto.substr(0, self.kursoro) + \
		nova_letero + \
		self.teksto.substr(self.kursoro+1, len(self.teksto))
	
func dekstremaldekstre(dekstre):
	# igas antauxen/malantauxen la kursoron
	self.kursoro += dekstre
	# limigas la maksimumon je la longeco de la teksto
	self.kursoro = min(self.kursoro, 24)
	# limigas la minimumon je nulo
	self.kursoro = max(self.kursoro, 0)