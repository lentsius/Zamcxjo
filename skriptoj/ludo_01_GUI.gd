extends Control

onready var l1_poentoj = $maldekstre/poentoj
onready var l2_poentoj = $dekstre/poentoj

func _ready():
	globala.gui = self
	pass

func _process(delta):
	gxisdatigi_tempon()
	l1_poentoj()
	l2_poentoj()
	
	kontroli_tempon()
	
func l1_poentoj():
	var nuntemppoentoj = int(l1_poentoj.text)
	var novaj_poentoj = lerp(nuntemppoentoj, globala.l1_poentoj, 0.9)
	l1_poentoj.text = str(int(novaj_poentoj))

func l2_poentoj():
	var nuntemppoentoj = int(l2_poentoj.text)
	var novaj_poentoj = lerp(nuntemppoentoj, globala.l2_poentoj, 0.9)
	l2_poentoj.text = str(int(novaj_poentoj))	
	
func gxisdatigi_tempon():
	if $lud_tempo.time_left != 0:
		$tempo.text = str(ceil($lud_tempo.time_left))

func _on_ludo_bone():
	pass # replace with function body


func _on_ludo_frue():
	pass # replace with function body


func _on_ludo_mojose():
	pass # replace with function body


func _on_komencekrano_start():
	$lud_tempo.start()

func kontroli_tempon():
	if $lud_tempo.time_left < 60 and $lud_tempo.time_left > 30:
		$tempo.modulate = Color(1,1,1)
		$tempo_animacio.playback_speed = 0.5
	if $lud_tempo.time_left < 30 and $lud_tempo.time_left > 10:
		$tempo_animacio.playback_speed = 1
		$tempo.modulate = Color(1,0.5,0)

	if $lud_tempo.time_left < 10:
		$tempo.modulate = Color(1,0,0)
		$tempo_animacio.playback_speed = 2
		
### anoncoj pro batoj
func l1_mistrafis():
	$'../sonoj/ooo'.play()
	l1_videbligi($maldekstre/centro/mistrafis)
	yield(get_tree().create_timer(1), "timeout")
	l1_malvidebligi($maldekstre/centro/mistrafis)
		
func ll1_frue():
	$'../sonoj/frue'.play()
	l1_videbligi($maldekstre/centro/frue)
	yield(get_tree().create_timer(1), "timeout")
	l1_malvidebligi($maldekstre/centro/frue)

	
func ll1_bone():
	$'../sonoj/bone'.play()
	l1_videbligi($maldekstre/centro/bone)
	yield(get_tree().create_timer(1), "timeout")
	l1_malvidebligi($maldekstre/centro/bone)

	
func ll1_mojose():
	$'../sonoj/mojose'.play()
	$'../sonoj/manklakoj'.play()
	$'../kamerao/kamerao_animacioj'.play("tremo")
	l1_videbligi($maldekstre/centro/mojose)
	yield(get_tree().create_timer(1), "timeout")
	l1_malvidebligi($maldekstre/centro/mojose)
		
	
func ll2_mistrafis():
	$'../sonoj/ooo'.play()
	l2_videbligi($dekstre/centro/mistrafis)
	yield(get_tree().create_timer(1), "timeout")
	l2_malvidebligi($dekstre/centro/mistrafis)

	
func ll2_frue():
	$'../sonoj/frue'.play()
	l2_videbligi($dekstre/centro/frue)
	yield(get_tree().create_timer(1), "timeout")
	l2_malvidebligi($dekstre/centro/frue)
		
func ll2_bone():
	$'../sonoj/bone'.play()
	l2_videbligi($dekstre/centro/bone)
	yield(get_tree().create_timer(1), "timeout")
	l2_malvidebligi($dekstre/centro/bone)

func ll2_mojose():
	$'../sonoj/mojose'.play()
	$'../sonoj/manklakoj'.play()
	$'../kamerao/kamerao_animacioj'.play("tremo")
	l2_videbligi($dekstre/centro/mojose)
	yield(get_tree().create_timer(1), "timeout")
	l2_malvidebligi($dekstre/centro/mojose)
		
func _on_lud_tempo_timeout():
	$tempo.text = "PUNKTO FINO"
	$'../sonoj/punktofino'.play()
	
func l1_malvidebligi(objekto):
	$maldekstre/centro/tvino.interpolate_property(objekto, "modulate", Color(1,1,1,1), Color(1,1,1,0), 2, Tween.TRANS_SINE, Tween.EASE_IN_OUT, 0)
	$maldekstre/centro/tvino.start()

func l1_videbligi(objekto):
	$maldekstre/centro/tvino.interpolate_property(objekto, "modulate", Color(1,1,1,0), Color(1,1,1,1), 0.5, Tween.TRANS_SINE, Tween.EASE_IN_OUT, 0)
	$maldekstre/centro/tvino.start()

func l2_malvidebligi(objekto):
	$dekstre/centro/tvino.interpolate_property(objekto, "modulate", Color(1,1,1,1), Color(1,1,1,0), 2, Tween.TRANS_SINE, Tween.EASE_IN_OUT, 0)
	$dekstre/centro/tvino.start()

func l2_videbligi(objekto):
	$dekstre/centro/tvino.interpolate_property(objekto, "modulate", Color(1,1,1,0), Color(1,1,1,1), 0.5, Tween.TRANS_SINE, Tween.EASE_IN_OUT, 0)
	$dekstre/centro/tvino.start()