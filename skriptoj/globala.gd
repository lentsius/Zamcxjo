extends Node

var rapideco = 1
var l1_poentoj = 0
var l2_poentoj = 0

var venkis = 0

var jam_ludis = false

var ludo = null
var gui = null

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	pass
	
func nuligi_vorton():
	ludo.vorto = null
	
func nuligi_poentojn(l):
	if l == 1:
		l1_poentoj = 0
	else:
		l2_poentoj = 0
		
func resxalti_ludstaton():
	l1_poentoj = 0
	l2_poentoj = 0
	venkis = 0
		
func l1_partikloj(kvanto, modo):
	ludo.l1_partikligxi(kvanto, modo)
	
func l2_partikloj(kvanto, modo):
	ludo.l2_partikligxi(kvanto, modo)	
	
func l1_mistrafis():
	gui.l1_mistrafis()
	
func l1_frue():
	gui.ll1_frue()
	
func l1_bone():
	gui.ll1_bone()
	
func l1_mojose():
	gui.ll1_mojose()

func l2_mistrafis():
	gui.ll2_mistrafis()
	
func l2_frue():
	gui.ll2_frue()
	
func l2_bone():
	gui.ll2_bone()
	
func l2_mojose():
	gui.ll2_mojose()