extends Control

var l1_pretas = false
var l2_pretas = false
var pauzite = false

var funkcio = false

var entabuligxas = false

func _ready():
	pass

func _process(delta):
	if funkcio:
		#sistemaj butonoj, helpaj precipe por menuoj
		if Input.is_action_just_pressed("start") or Input.is_action_just_pressed("start_l2"):
			if entabuligxas:
				return
			if l1_pretas and l2_pretas:
				globala.resxalti_ludstaton()
				l1_pretas = false
				l2_pretas = false
				get_tree().paused = false
				self.visible = false
				pauzite = false
				$'../ludo/pauztempo'.start()
				get_tree().change_scene('res://scenoj/gustamalgusta.tscn')
				
	
		if Input.is_action_just_pressed("select") or Input.is_action_just_pressed("ui_accept"):
			if entabuligxas:
				return			
			if pauzite:
				$sube_maldekstre/CenterContainer/TextureButton.pressed = !$sube_maldekstre/CenterContainer/TextureButton.pressed
				l1_pretas = !l1_pretas
			
		if Input.is_action_just_pressed("select_l2") or Input.is_action_just_pressed("ui_accept"):
			if entabuligxas:
				return
			if pauzite:
				$sube_maldekstre2/CenterContainer/TextureButton.pressed = !$sube_maldekstre2/CenterContainer/TextureButton.pressed
				l2_pretas = !l2_pretas
		
		if l1_pretas and l2_pretas:
#			$gajnanto/Label3.visible = true
			pass
		else:
#			$gajnanto/Label3.visible = false
			pass

		
func kontroli_poentojn():
	if globala.l1_poentoj > globala.l2_poentoj:
		globala.venkis = 1
		$VBoxContainer/gajnanto.text = "LUDANTO 1"
		$VBoxContainer/poentoj.text = str(globala.l1_poentoj)
		$'../sonoj/l1_venkas'.play()
		if $VBoxContainer/poenttabulo.entabuligx_kontrolo() < 9:
			$VBoxContainer/priskribo.text = "ENPOENTABULIGXIS!"
			entabuligxas = true
		else:
			$VBoxContainer/priskribo.text = "VENKIS!"
			
	else:
		globala.venkis = 2
		$VBoxContainer/gajnanto.text = "LUDANTO 2"
		$VBoxContainer/poentoj.text = str(globala.l2_poentoj)
		$'../sonoj/l2_venkas'.play()
		if $VBoxContainer/poenttabulo.entabuligx_kontrolo() < 9:
			$VBoxContainer/priskribo.text = "ENPOENTABULIGXIS!"
			entabuligxas = true
		else:
			$VBoxContainer/priskribo.text = "VENKIS!"
		
func apero():
	kontroli_poentojn()
	pauzite = true
	funkcio = true
	$'../sonoj/aplaudo'.play()
	

func _on_poenttabulo_entabuligxis():
	entabuligxas = false
	print("Signalo funkciis, dosiero konservis, ludo daurigas.")


func _on_poenttabulo_entabuligxis_l1():
	entabuligxas = false
	print("Signalo funkciis, dosiero konservis, ludo daurigas.")


func _on_poenttabulo_entabuligxis_l2():
	entabuligxas = false
	print("Signalo funkciis, dosiero konservis, ludo daurigas.")
