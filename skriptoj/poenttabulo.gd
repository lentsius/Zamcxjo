extends ColorRect

var ludantoj = []
var poentoj = []

var leg_ludantoj = []
var leg_poentoj = []

var loko = 0

var entabuligxas = 0

var venkpoentoj = 0

var skribilo

var venkanto

signal entabuligxis_l2
signal entabuligxis_l1

func _ready():
	
	skribilo = preload('res://skriptoj/skribilo.gd').new()
	
	poentoj = $loko/h_skatolo/poentoj.get_children()
	ludantoj = $loko/h_skatolo/ludantoj.get_children()	
	plenigxi()
	vortigxi()

func _process(delta):
	if entabuligxas:
		var letera_sxangxo = false
		var kursora_sxangxo = false	
		if venkanto == 1:	
			if Input.is_action_just_pressed("up"):
				skribilo.supresube(-1)
				letera_sxangxo = true
			if Input.is_action_just_pressed("down"):
				skribilo.supresube(1)
				letera_sxangxo = true
			if Input.is_action_just_pressed("left"):
				skribilo.dekstremaldekstre(-1)
				kursora_sxangxo = true
			if Input.is_action_just_pressed("right"):
				skribilo.dekstremaldekstre(1)
				kursora_sxangxo = true
				
			if Input.is_action_just_pressed("select"):
				ludantoj[loko].modulate = Color(1,1,1)
				poentoj[loko].modulate = Color(1,1,1)				
				entabuligxas = 0
				konservi_tabulon()
				emit_signal("entabuligxis_l1")
				print("SIGNALO")
				$klaku.visible = false
			
		if venkanto == 2:	
			if Input.is_action_just_pressed("up_l2"):
				skribilo.supresube(-1)
				letera_sxangxo = true
			if Input.is_action_just_pressed("down_l2"):
				skribilo.supresube(1)
				letera_sxangxo = true
			if Input.is_action_just_pressed("left_l2"):
				skribilo.dekstremaldekstre(-1)
				kursora_sxangxo = true
			if Input.is_action_just_pressed("right_l2"):
				skribilo.dekstremaldekstre(1)
				kursora_sxangxo = true
				
			if Input.is_action_just_pressed("select_l2"):
				ludantoj[loko].modulate = Color(1,1,1)
				poentoj[loko].modulate = Color(1,1,1)
				entabuligxas = 0
				konservi_tabulon()
				emit_signal("entabuligxis_l2")
				$klaku.visible = false

			
		if letera_sxangxo:
			ludantoj[loko].text = skribilo.teksto
		if kursora_sxangxo:
			pass

func plenigxi():
	var dosiero = File.new()
	dosiero.open("res://poenttabulo/poenttabulo.txt", File.READ)
	while !dosiero.eof_reached():
		leg_ludantoj.append(str(dosiero.get_line()))
		leg_poentoj.append(int(dosiero.get_line()))
	print(leg_ludantoj.size())
	if leg_ludantoj.size() > 9:
		leg_ludantoj.resize(10)
	if leg_poentoj.size() > 9:
		leg_poentoj.resize(10)
	dosiero.close()

func vortigxi():
	var x = 0
	var longeco = leg_ludantoj.size()
#	for i in leg_ludantoj:
#		ludantoj[leg_ludantoj.find(i)].text = str(i)
#	for i in leg_poentoj:
#		poentoj[leg_poentoj.find(i)].text = str(i)
	while x < longeco:
		ludantoj[x].text = str(leg_ludantoj[x])
		poentoj[x].text = str(leg_poentoj[x])
		x = x + 1
	
	print(leg_ludantoj)
	print(leg_poentoj)

func entabuligx_kontrolo():
	venkanto = globala.venkis
	var ludantpoentoj
	loko = 0
	if venkanto == 1:
		venkpoentoj = globala.l1_poentoj
		ludantpoentoj = globala.l1_poentoj
		for i in leg_poentoj:
			if ludantpoentoj < i:
				loko += 1
	else:
		venkpoentoj = globala.l2_poentoj
		ludantpoentoj = globala.l2_poentoj
		for i in leg_poentoj:
			if ludantpoentoj < i:
				loko += 1
	print(loko)
	
	if loko < 9:
		entabuligxas = 1
		$klaku.visible = true
		ludantoj[loko].text = skribilo.teksto
		enpoentabuligxi(loko)
		
	return(loko)
	
func enpoentabuligxi(vicloko):

	leg_ludantoj.insert(vicloko, "")
	ludantoj[vicloko].modulate = Color(1,0.5,0)
	
	leg_poentoj.insert(vicloko, venkpoentoj)
	poentoj[vicloko].modulate = Color(1,0.5,0)
	
	if leg_ludantoj.size() > 9:
		leg_ludantoj.pop_back()
	if leg_poentoj.size() > 9:	
		leg_poentoj.pop_back()
	vortigxi()
	ludantoj[loko].text = skribilo.teksto
	
func konservi_tabulon():
	
	var dosiero = File.new()
	dosiero.open("res://poenttabulo/poenttabulo.txt", File.WRITE)
	
	for i in ludantoj:
		var numero = ludantoj.find(i)
		dosiero.store_line(ludantoj[numero].text)
		dosiero.store_line(poentoj[numero].text)
	dosiero.close()
	print("koservite!")
