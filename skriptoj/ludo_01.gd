extends Spatial

var frua_vorto = null
var vorto = null
var modo = ""

var frupoentoj = 50
var bonpoentoj = 100
var mojospoentoj = 300

var pauzeblas = false

signal frue
signal bone
signal mojose

var vortoj = []

var l1_partikloj
var l2_partikloj

var pasinta_l1_atingo = ""
var pasinta_l2_atingo = ""

var l1_kombo = 0
var l2_kombo = 0

var l1_vertikale = 0
var l2_vertikale = 0

func _ready():
	globala.ludo = self
	l1_partikloj = $l1_partikloj.get_children()
	l2_partikloj = $l2_partikloj.get_children()

func _process(delta):
	gxisdatigi_vortojn()
	ludanto_unu()
	ludanto_du()
	pauzi()
	
func pauzi():
	if Input.is_action_just_pressed("start") or Input.is_action_just_pressed("start_l2"):
		ludu_sonon("klako")		
		if pauzeblas:
			get_tree().paused = true
			$'../pauzo'.visible = true
			$'../pauzo'.pauzite = true
	
func gxisdatigi_vortojn():
	if $frue.get_overlapping_bodies().size() > 0:
		vorto = $frue.get_overlapping_bodies()[0]
		modo = "frue"
		
	if $bone.get_overlapping_bodies().size() > 0:
		vorto = $bone.get_overlapping_bodies()[0]
		modo = "bone"
		
	if $mojose.get_overlapping_bodies().size() > 0:
		vorto = $mojose.get_overlapping_bodies()[0]
		modo = "mojose"
	
	if $bone2.get_overlapping_bodies().size() > 0:
		vorto = $bone2.get_overlapping_bodies()[0]
		modo = "bone"
	
	if $maltrafo.get_overlapping_bodies().size() > 0:
		$maltrafo.get_overlapping_bodies()[0].maltrafi()
	
func ludanto_unu():	
	## Ludanto unu klaksonoj
	if Input.is_action_just_pressed("right") or Input.is_action_just_pressed("left") or Input.is_action_just_pressed("down") or Input.is_action_just_pressed("up"):
		ludu_sonon("klako")
		print(mojospoentoj, (mojospoentoj + (frupoentoj * l1_kombo)), l1_kombo)
	
	## MALDEKSTREN!!
	
	if Input.is_action_just_pressed("right") and vorto != null:
		if !vorto.l1_klakis:
			if modo == "frue":
				vorto.frue(1, 1, frupoentoj)
				pasinta_l1_atingo = "frue"
				l1_kombo = 0

			if modo == "bone":
				vorto.bone(1, 1, bonpoentoj)
				pasinta_l1_atingo = "bone"
				l1_kombo = 0
				
			if modo == "mojose":
				if pasinta_l1_atingo == "mojose":
					l1_kombo += 1
					vorto.mojose(1, 1, (mojospoentoj + (bonpoentoj * l1_kombo * 2)))
				else:
					vorto.mojose(1, 1, mojospoentoj)
				pasinta_l1_atingo = "mojose"

	## DEKSTREN!!
	if Input.is_action_just_pressed("left") and vorto != null:
		if !vorto.l1_klakis:
			if modo == "frue":
				vorto.frue(1, 2, frupoentoj)
				pasinta_l1_atingo = "frue"
				l1_kombo = 0
				
			if modo == "bone":
				vorto.bone(1, 2, bonpoentoj)
				pasinta_l1_atingo = "bone"
				l1_kombo = 0
				
			if modo == "mojose":
				if pasinta_l1_atingo == "mojose":
					l1_kombo += 1
					vorto.mojose(1, 2, (mojospoentoj + (bonpoentoj * l1_kombo * 2)))
				else:
					vorto.mojose(1, 2, mojospoentoj)
				pasinta_l1_atingo = "mojose"


	## VERTIKALE!! IOM DA LOGIKO!!!
	if Input.is_action_just_pressed("down"):
		if vorto == null:
			print("senvorta klako")
			return
			
		if l1_vertikale == 0:
			l1_vertikale = 1
			$l1_pasxtempilo.start()
			
	if Input.is_action_just_pressed("up"):
		if vorto == null:
			print("senvorta klako")
			return
		
		if l1_vertikale == 0:
			$l1_pasxtempilo.start()
			l1_vertikale = 2

	if Input.is_action_just_released("up") or Input.is_action_just_released("down"):
		l1_vertikale = 0

	if l1_vertikale == 1:
		if Input.is_action_just_pressed("up"):
			vertikala_pasxklako_l1()

	if l1_vertikale == 2:
		if Input.is_action_just_pressed("down"):
			vertikala_pasxklako_l1()

func vertikala_pasxklako_l1():
	if !vorto.l1_klakis:
		if modo == "frue":
			vorto.frue(1, 3, frupoentoj)
			pasinta_l1_atingo = "frue"
			l1_kombo = 0
			
		if modo == "bone":
			vorto.bone(1, 3, bonpoentoj)
			pasinta_l1_atingo = "bone"
			l1_kombo = 0
			
		if modo == "mojose":
			if pasinta_l1_atingo == "mojose":
				l1_kombo += 1
				vorto.mojose(1, 3, (mojospoentoj + (bonpoentoj * l1_kombo * 2)))
			else:
				vorto.mojose(1, 3, mojospoentoj)
			pasinta_l1_atingo = "mojose"

func ludanto_du():
	
	## Ludanto unu klaksonoj
	if Input.is_action_just_pressed("right_l2") or Input.is_action_just_pressed("left_l2") or Input.is_action_just_pressed("down_l2") or Input.is_action_just_pressed("up_l2"):
		ludu_sonon("klako")	
	
	## MALDEKSTREN!!
	if Input.is_action_just_pressed("right_l2") and vorto != null:
		if !vorto.l2_klakis:
			if modo == "frue":
				vorto.frue(2, 1, frupoentoj)
				l2_kombo = 0
			if modo == "bone":
				vorto.bone(2, 1, bonpoentoj)
				l2_kombo = 0
				
			if modo == "mojose":
				if pasinta_l2_atingo == "mojose":
					l2_kombo += 1
					vorto.mojose(2, 1, (mojospoentoj + (bonpoentoj * l2_kombo * 2)))
				else:
					vorto.mojose(2, 1, mojospoentoj)
				pasinta_l2_atingo = "mojose"

	## DEKSTREN!!
	if Input.is_action_just_pressed("left_l2") and vorto != null:
		if !vorto.l2_klakis:
			if modo == "frue":
				vorto.frue(2, 2, frupoentoj)
				l2_kombo = 0
				
			if modo == "bone":
				vorto.bone(2, 2, bonpoentoj)
				l2_kombo = 0
				
			if modo == "mojose":
				if pasinta_l2_atingo == "mojose":
					l2_kombo += 1
					vorto.mojose(2, 2, (mojospoentoj + (bonpoentoj * l2_kombo * 2)))
				else:
					vorto.mojose(2, 2, mojospoentoj)
				pasinta_l2_atingo = "mojose"

	## VERTIKALE!! IOM DA LOGIKO!!!
	if Input.is_action_just_pressed("down_l2"):
		if vorto == null:
			print("senvorta klako")
			return

		if l2_vertikale == 0:
			$l2_pasxtempilo.start()
			l2_vertikale = 1
			
	if Input.is_action_just_pressed("up_l2"):
		if vorto == null:
			print("senvorta klako")
			return

		if l2_vertikale == 0:
			$l2_pasxtempilo.start()
			l2_vertikale = 2

	if Input.is_action_just_released("up_l2") or Input.is_action_just_released("down_l2"):
		$l2_pasxtempilo.stop()
		l2_vertikale = 0

	if l2_vertikale == 1:
		if Input.is_action_just_pressed("up_l2"):
			vertikala_pasxklako_l2()

	if l2_vertikale == 2:
		if Input.is_action_just_pressed("down_l2"):
			vertikala_pasxklako_l2()

func vertikala_pasxklako_l2():
	if !vorto.l2_klakis:
		if modo == "frue":
			vorto.frue(2, 3, frupoentoj)
			pasinta_l2_atingo = "frue"
			l2_kombo = 0
			
		if modo == "bone":
			vorto.bone(2, 3, bonpoentoj)
			pasinta_l2_atingo = "bone"
			l2_kombo = 0
			
		if modo == "mojose":
			if pasinta_l2_atingo == "mojose":
				l2_kombo += 1
				vorto.mojose(2, 3, (mojospoentoj + (bonpoentoj * l2_kombo * 2)))
			else:
				vorto.mojose(2, 3, mojospoentoj)
			pasinta_l2_atingo = "mojose"

func l1_partikligxi(kvanto, modo):
	if modo == "frue":
		for i in l1_partikloj:
			var mat = i.process_material
			mat.scale = 0.5
			i.amount = kvanto
			i.emitting = true
			
	if modo == "bone":
		for i in l1_partikloj:
			var mat = i.process_material
			mat.scale = 1
			i.amount = kvanto
			i.emitting = true
			
	if modo == "mojose":
		for i in l1_partikloj:
			var mat = i.process_material
			mat.scale = 1.5
			i.amount = kvanto
			i.emitting = true
		
func l2_partikligxi(kvanto, modo):
	if modo == "frue":
		for i in l2_partikloj:
			var mat = i.process_material
			mat.scale = 0.5
			i.amount = kvanto
			i.emitting = true
			
	if modo == "bone":
		for i in l2_partikloj:
			var mat = i.process_material
			mat.scale = 1
			i.amount = kvanto
			i.emitting = true
			
	if modo == "mojose":
		for i in l2_partikloj:
			var mat = i.process_material
			mat.scale = 1.5
			i.amount = kvanto
			i.emitting = true


func _on_Timer_timeout():
	$pauztempo.stop()
	pauzeblas = true
	
func fino():
	$fin_tempilo.start()

func _on_fin_tempilo_timeout():
	get_tree().paused = true
	globala.jam_ludis = true
	$'../fino'.visible = true
	$'../fino'.apero()
	$'../sonoj/muziko'.nunakanto.stop()
	
func ludu_sonon(sono):
	if sono == "klako":
		$'../sonoj/klako'.play()
	
	
func _on_l2_pasxtempilo_timeout():
	l2_vertikale = 0

func _on_l1_pasxtempilo_timeout():
	l1_vertikale = 0


func _on_bone2_body_exited(body):
	vorto = null
