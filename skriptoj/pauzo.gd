extends Control

var l1_pretas = false
var l2_pretas = false
var pauzite = false

func _ready():
	pass

func _process(delta):
	#sistemaj butonoj, helpaj precipe por menuoj
	if Input.is_action_just_pressed("start") or Input.is_action_just_pressed("start_l2"):
		if l1_pretas and l2_pretas:
			l1_pretas = false
			l2_pretas = false
			get_tree().paused = false
			self.visible = false
			pauzite = false
			$'../ludo/pauztempo'.start()
			

	if Input.is_action_just_pressed("select") or Input.is_action_just_pressed("ui_accept"):
		if pauzite:
			$sube_maldekstre/CenterContainer/TextureButton.pressed = !$sube_maldekstre/CenterContainer/TextureButton.pressed
			l1_pretas = !l1_pretas
		
	if Input.is_action_just_pressed("select_l2") or Input.is_action_just_pressed("ui_accept"):
		if pauzite:
			$sube_maldekstre2/CenterContainer/TextureButton.pressed = !$sube_maldekstre2/CenterContainer/TextureButton.pressed		
			l2_pretas = !l2_pretas
		
	if l1_pretas and l2_pretas:
		$komencu.visible = true
	else:
		$komencu.visible = false